
<?php

	class ConversionDollar{
		const TAUX_DOLLAR_TO_EURO = 9/10;
		
		function toEuro($dollar = 0) {
			return self::TAUX_DOLLAR_TO_EURO * $dollar;
		}
	}

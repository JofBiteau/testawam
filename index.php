<!DOCTYPE html>
<html>
<body>

<?php
    require 'ConversionDollar.php';

	$resultatConversion = new ConversionDollar();

	if (!empty($_POST['euro']) && is_numeric($_POST['euro'])){
	    $euro = $_POST['euro'];
    } else {
	    $euro = 0;
    }

    if (!empty($_POST['dollar']) && is_numeric($_POST['dollar'])){
        $dollar = $_POST['dollar'];
    } else {
        $dollar = 0;
    }

    $resultat = $resultatConversion->toEuro($dollar) + $euro;

	?>
		
	<h1>X $ + Y € = Z €</h1>

	<form action="index.php" method="POST">
	  <input type="float" id="euro" value="<?php echo isset($euro)? $euro : 0;?>" name="euro">
	  <label for="points"> euro + </label>
	  <input type="float" id="dollar" value="<?php echo isset($dollar)? $dollar : 0;?>" name="dollar">
	  <label for="points"> dollar</label>
	  
	  <input type="submit" text="Calculer">
	</form>

	<span id="resultat">
		<h4>Resultat: </h4>

	<?php 
		if (isset($resultat)){
			echo $resultat . "€"; 
		}
		?>

</body>
</html>

	
	